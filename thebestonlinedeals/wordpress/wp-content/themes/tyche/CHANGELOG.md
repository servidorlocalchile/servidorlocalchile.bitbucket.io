Tyche v1.0.7
- Updated welcome screen
- Added a notice in the frontend if user does not have widgets in the content area
- Removed documentation section
- Bug fixes

Tyche v1.0.6
- Bug Fixes

Tyche v1.0.5
- Bug Fixes
- https://themes.trac.wordpress.org/ticket/43404#comment:6

Tyche v1.0.4
- Bug fixes
- https://themes.trac.wordpress.org/ticket/43404#comment:3

Tyche v1.0.2
- Bug fixes ( https://github.com/puikinsh/tyche/pull/7 )
- Update theme screenshot

Tyche v1.0.2
- Bug fixes for mobile environments

Tyche v1.0.1
- Bug fixing found when creating the demo content

Tyche v1.0.0 
- Release to w.org for theme review

